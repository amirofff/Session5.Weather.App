package amirofff.test.session4;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class OghatSharyActivity extends AppCompatActivity {

    TextView sunsetView;
    TextView sunriseView;
    TextView zohrView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_oghat_shary);

        sunsetView = (TextView) findViewById(R.id.sunSet);
        sunriseView = (TextView) findViewById(R.id.sunRise);
        zohrView = (TextView) findViewById(R.id.zohr);

        findViewById(R.id.showOghagt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getOghat();
            }
        });


    }

    void getOghat() {
        new Thread(new Runnable() {
            @Override
            public void run() {

            try{
                URL obj = new URL("http://api.aladhan.com/timings/1398332113?latitude=51.508515&longitude=-0.1254872&timezonestring=Europe/London&method=2");
                HttpURLConnection con = (HttpURLConnection) obj.openConnection();
                con.setRequestMethod("GET");
                con.setRequestProperty("User-Agent", "Mozilla/5.0");
                int responseCode = con.getResponseCode();
                if (responseCode == HttpURLConnection.HTTP_OK) {
                    BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
                    String inputLine;
                    StringBuffer response = new StringBuffer();
                    while ((inputLine = in.readLine()) != null) {
                        response.append(inputLine);
                    }

                    parseServerResponse(response.toString());

                }
            }catch (Exception e){
                Toast.makeText(OghatSharyActivity.this, "Error in connecting to website", Toast.LENGTH_SHORT).show();

            }


            }
        }).start();

    }



    void parseServerResponse(String response){

        Log.d("webservice_debug" , response);

        try {
            JSONObject allObj = new JSONObject(response);
            String dataStr= allObj.getString("data");
            JSONObject dataObj = new JSONObject(dataStr);

            String timingsStr = dataObj.getString("timings");
            JSONObject timingsObj = new JSONObject(timingsStr);

            final String sunset = timingsObj.getString("Sunset");
            final String sunrise = timingsObj.getString("Sunrise");
            final String zohr = timingsObj.getString("Dhuhr");

         runOnUiThread(new Runnable() {
             @Override
             public void run() {
                 sunsetView.setText(sunset);
                 sunriseView.setText(sunrise);
                 zohrView.setText(zohr);
             }
         });



        } catch (JSONException e) {
            e.printStackTrace();
        }


    }


}
