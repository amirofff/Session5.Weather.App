package amirofff.test.session4;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.GridView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import amirofff.test.session4.adapters.channelsAdapter;
import amirofff.test.session4.models.ChannelModel;

public class ChannelListActivity extends AppCompatActivity {
    ListView channelList;
    GridView myGrid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_channel_list);


        channelList = (ListView) findViewById(R.id.channelList);
        myGrid = (GridView) findViewById(R.id.myGrid);

        ChannelModel channel1 = new ChannelModel("شبکه یک", "http://staticfiles.telewebion.com/web/content_images/channel_images/thumbs/new/240/v3/tv1.png", 1);
        ChannelModel channel2 = new ChannelModel("شبکه دو", "http://staticfiles.telewebion.com/web/content_images/channel_images/thumbs/new/240/v3/tv2.png", 2);
        ChannelModel channel3 = new ChannelModel("شبکه سه", "http://staticfiles.telewebion.com/web/content_images/channel_images/thumbs/new/240/v3/tv3.png", 3);
        ChannelModel channel4 = new ChannelModel("شبکه چهار", "http://staticfiles.telewebion.com/web/content_images/channel_images/thumbs/new/240/v3/tv4.png", 4);
        ChannelModel channel5 = new ChannelModel("شبکه پنج", "http://staticfiles.telewebion.com/web/content_images/channel_images/thumbs/new/240/v3/tehran.png", 5);
        ChannelModel channel6 = new ChannelModel("شبکه خبر", "http://staticfiles.telewebion.com/web/content_images/channel_images/thumbs/new/240/v3/irinn.png", 6);
        ChannelModel channel7 = new ChannelModel("شبکه آموزش", "http://staticfiles.telewebion.com/web/content_images/channel_images/thumbs/new/240/v3/amouzesh.png", 7);
        ChannelModel channel8 = new ChannelModel("شبکه قرآن", "http://staticfiles.telewebion.com/web/content_images/channel_images/thumbs/new/240/v3/quran.png", 8);
        ChannelModel channel9 = new ChannelModel("شبکه نمایش", "http://staticfiles.telewebion.com/web/content_images/channel_images/thumbs/new/240/v3/namayesh.png", 9);


        List<ChannelModel> channels = new ArrayList<>();
        channels.add(channel1);
        channels.add(channel2);
        channels.add(channel3);
        channels.add(channel4);
        channels.add(channel5);
        channels.add(channel6);
        channels.add(channel7);
        channels.add(channel8);
        channels.add(channel9);


        channelsAdapter adapterGrid = new channelsAdapter(this, channels, false);
        myGrid.setAdapter(adapterGrid);

        channelsAdapter adapterList = new channelsAdapter(this, channels, true);
        channelList.setAdapter(adapterList);


    }
}
